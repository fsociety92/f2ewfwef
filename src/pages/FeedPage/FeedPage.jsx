import React from "react";
import { useSelector } from "react-redux";
import { Link } from "react-router-dom";

function FeedPage() {
  const { posts } = useSelector((state) => state.posts);

  return (
    <div>
      <div>FeedPage</div>
      <div>
        {posts.map((post) => (
          <div key={post.id}>
            <Link />
            <div>{post.text}</div>
            {post.image && <img src={post.image} alt="" />}
          </div>
        ))}
      </div>
    </div>
  );
}

export default FeedPage;
