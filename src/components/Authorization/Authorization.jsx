import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useSnackbar } from "notistack";

import Input from "../Input";
import Button from "../Button/Button";
import { auth, dismissError } from "../../store/userSlice";

import s from "./Authorization.module.scss";

const endpoint = "https://jsonplaceholder.typicode.com";

function Authorization() {
  const dispatch = useDispatch();
  const { enqueueSnackbar } = useSnackbar();

  const { error } = useSelector((store) => store.user);

  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [users, setUsers] = useState({});

  const onEmailChange = (event) => {
    setEmail(event.target.value);
  };
  const onPasswordChange = (event) => {
    setPassword(event.target.value);
  };

  useEffect(() => {
    if (error) {
      enqueueSnackbar({ message: error, variant: "error" });
      dispatch(dismissError())
    }
  }, [error]);

  const onSubmit = async (event) => {
    event.preventDefault();

    if (!email) {
      enqueueSnackbar({ message: "There is no E-mail", variant: "error" });
      return;
    }
    if (!password) {
      enqueueSnackbar({ message: "There is no Password", variant: "error" });
      return;
    }

    dispatch(auth({ login: email, password }));

    // const response = await fetch(`${endpoint}/users`, {
    //   method: "POST",
    //   body: JSON.stringify({
    //     email,
    //     password,
    //   }),
    //   headers: {
    //     "Content-Type": "application/json",
    //   },
    // });

    // const data = await response.json();
    // setUsers(data);
    // console.log(data);
  };

  return (
    <form className={s.email_address} onSubmit={onSubmit}>
      <div className={s.inputs}>
        <Input
          value={email}
          onChange={onEmailChange}
          placeholder="Your E-mail"
          label="Email address"
        />
        <Input
          value={password}
          onChange={onPasswordChange}
          placeholder="Your Password"
          label="Password"
        />
      </div>
      <Button type="submit">Sign in</Button>
    </form>
  );
}

export default Authorization;
