import { createSlice } from "@reduxjs/toolkit";

const users = [
  {
    login: "bmw",
    password: "qwerty",
  },
  {
    login: "bmw1",
    password: "qwerty1",
  },
  {
    login: "bmw2",
    password: "qwerty2",
  },
];

const UserSlice = createSlice({
  name: "user",
  initialState: {
    user: {
      login: "bmw",
      password: "qwerty",
    },
    users,
    loading: false,
    error: null,
  },
  reducers: {
    dismissError(state){
      state.error = null
    },
    auth(state, action) {
      const user = users.find(
        (elem) =>
          elem.login === action.payload.login &&
          elem.password === action.payload.password
      );
      if (!user) {
        state.error = "There is no such user:(";
      }
      if (user) {
        state.user = user;
        state.error = null;
      }
    },
    logOut(state) {
      state.user = null;
    },
    changeUser(state, action) {
      state.user.name = action.payload
    },
  },
});

export const { auth, logOut, dismissError } = UserSlice.actions;

export default UserSlice;

// const [state, setState] = useState()

// const {user, loading, error} = useSelector(state => state.user)

// const dispatch = useDispatch()
// // click logout
// dispatch(logOut({id: 12}))
// dispatch({type: "user/logOut", payload: {id: 12}})

// const useLogOut = () => {
//     const dispatch = useDispatch()

//     return dispatch(logOut())
// }

//

// const logOut = useLogOut()

// logOut()
