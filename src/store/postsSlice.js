import { createSlice } from "@reduxjs/toolkit";

const posts = [
  {
    id: 1,
    text: "some text 1",
    image:
      "https://hips.hearstapps.com/hmg-prod/images/2024-bmw-5-series-101-1676576715.jpg?crop=0.748xw:0.882xh;0.173xw,0.118xh&resize=640:*",
  },
  {
    id: 2,
    text: "some text 2",
    image:
      "https://hips.hearstapps.com/hmg-prod/images/2024-bmw-5-series-101-1676576715.jpg?crop=0.748xw:0.882xh;0.173xw,0.118xh&resize=640:*",
  },
  {
    id: 3,
    text: "some text 3",
    image:
      "https://hips.hearstapps.com/hmg-prod/images/2024-bmw-5-series-101-1676576715.jpg?crop=0.748xw:0.882xh;0.173xw,0.118xh&resize=640:*",
  },
];

const PostsSlice = createSlice({
  name: "posts",
  initialState: {
    posts,
  },
  reducers: {},
});

export default PostsSlice;
